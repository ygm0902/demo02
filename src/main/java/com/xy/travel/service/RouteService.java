package com.xy.travel.service;

import com.xy.travel.pojo.PageBean;
import com.xy.travel.pojo.Route;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

public interface RouteService {
    Map<String,List<Route>> careChoose();

    PageBean<Route> findByPage(String cid, Integer currentPage, Integer pageSize, String keyword);

    Route findByRid(String rid) throws InvocationTargetException, IllegalAccessException;
}
