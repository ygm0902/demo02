package com.xy.travel.service;

import com.xy.travel.exception.*;
import com.xy.travel.pojo.User;

public interface UserService {

    User findUserByUsername(String username);

    void doRegister(User user) throws Exception;

    void active(String code) throws AlreadyActiveException, ErrorCodeException;

    User doLogin(String username, String password) throws UnActivedException, ErrorPasswordException, ErrorUsernameException;
}
