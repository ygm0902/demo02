package com.xy.travel.service;

import com.xy.travel.pojo.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();

}
