package com.xy.travel.service.Impl;

import com.xy.travel.dao.RouteDao;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.PageBean;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.RouteImg;
import com.xy.travel.service.RouteService;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RouteServiceImpl implements RouteService {
    private RouteDao routeDao = (RouteDao) BeanFactory.getBean("routeDao");

    @Override
    public Map<String, List<Route>> careChoose() {
        //1.获取人气旅游集合
        List<Route> popList = routeDao.findPopList();
        //2.获取获取最新旅游路线集合
        List<Route> newestList = routeDao.findNewestList();
        //3.获取主题旅游路线集合
        List<Route> themeList = routeDao.findThemeList();
        HashMap<String, List<Route>> map = new HashMap<>();
        map.put("popList", popList);
        map.put("newestList", newestList);
        map.put("themeList", themeList);
        return map;
    }

    @Override
    public PageBean<Route> findByPage(String cid, Integer currentPage, Integer pageSize, String keyword) {
        PageBean<Route> pageBean = new PageBean<>();
        //设置当前页
        pageBean.setCurrentPage(currentPage);
        //设置每页显示的数量
        pageBean.setPageSize(pageSize);
        //dao层求得总数量
        Integer totalSize = routeDao.findTotalSize(cid, keyword);
        pageBean.setTotalSize(totalSize);
        //求得总页数
        Integer totalPage = totalSize % pageSize == 0 ? totalSize / pageSize : totalSize / pageSize + 1;
        pageBean.setTotalPage(totalPage);
        //获得当前页的集合
        List<Route> list = routeDao.findRouteListByPage(cid, currentPage, pageSize, keyword);
        pageBean.setList(list);
        return pageBean;
    }

    @Override
    public Route findByRid(String rid) throws InvocationTargetException, IllegalAccessException {
        Route route = routeDao.findByRid(rid);
        List<RouteImg> routeImgList = routeDao.findRouteImgListByRid(rid);
        route.setRouteImgList(routeImgList);
        return route;
    }
}
