package com.xy.travel.service.Impl;

import com.xy.travel.constant.Constant;
import com.xy.travel.dao.UserDao;
import com.xy.travel.exception.*;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.factory.MailUtil;
import com.xy.travel.factory.Md5Util;
import com.xy.travel.factory.UuidUtil;
import com.xy.travel.pojo.User;
import com.xy.travel.service.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao = (UserDao) BeanFactory.getBean("userDao");

    @Override
    public User findUserByUsername(String username) {
        return userDao.findUserByUsername(username);
    }

    @Override
    public void doRegister(User user) throws Exception {
        //获得用户输入的密码
        String password = user.getPassword();
        password = Md5Util.encodeByMd5(password);
        user.setPassword(password);

        //设置是否激活
        user.setStatus(Constant.UNACTIVE);
        user.setCode(UuidUtil.getUuid());

        //调用dao层
        userDao.saveUser(user);

        //发送激活码到邮箱
        MailUtil.sendMail(user.getEmail(), "欢迎" + user.getName() + "注册黑马旅游网，请点击<a href='http://localhost:8080/user?action=active&code=" + user.getCode() + "'>激活</a>");

    }

    @Override
    public void active(String code) throws AlreadyActiveException, ErrorCodeException {
        User user = userDao.findUserByCode(code);
        if (user != null) {
            //激活码有用
            if (!user.getStatus().equals(Constant.ACTIVE)) {
                //说明没有被激活
                //设置激活
                user.setStatus(Constant.ACTIVE);
                //调用dao更改用户信息
                userDao.updateUser(user);
            } else {
                //说明已经被激活
                throw new AlreadyActiveException();
            }
        } else {
            //说明激活码被篡改
            throw new ErrorCodeException();
        }
    }

    @Override
    public User doLogin(String username, String password) throws UnActivedException, ErrorPasswordException, ErrorUsernameException {
        User user = userDao.findUserByUsername(username);
        if (user != null) {
            //说明用户存在
            //校验密码
            try {
                password = Md5Util.encodeByMd5(password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (password.equals(user.getPassword())) {
                //说明密码正确
                //校验是否激活
                if (user.getStatus().equals(Constant.ACTIVE)) {
                    //激活le
                    return user;
                } else {
                    //未激活
                    throw new UnActivedException();
                }
            } else {
                //密码错误
                throw new ErrorPasswordException();
            }
        } else {
            //用户不存在
            throw new ErrorUsernameException();
        }
    }
}
