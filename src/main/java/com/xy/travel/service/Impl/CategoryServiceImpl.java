package com.xy.travel.service.Impl;

import com.xy.travel.dao.CategoryDao;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.Category;
import com.xy.travel.service.CategoryService;

import java.util.List;

public class CategoryServiceImpl implements CategoryService {
    private CategoryDao categoryDao = (CategoryDao) BeanFactory.getBean("categoryDao");

    @Override
    public List<Category> findAll() {
        return categoryDao.findAll();
    }
}
