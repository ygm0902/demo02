package com.xy.travel.service.Impl;

import com.xy.travel.dao.FavoriteDao;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.Favorite;
import com.xy.travel.pojo.PageBean;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.User;
import com.xy.travel.service.FavoriteService;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class FavoriteServiceImpl implements FavoriteService {
    private FavoriteDao favoriteDao = (FavoriteDao) BeanFactory.getBean("favoriteDao");

    @Override
    public Favorite findIsFavorite(String rid, User user) {
        return favoriteDao.findIsFavorite(rid, user);
    }

    @Override
    public Integer addFavorite(String rid, User user) {
        Integer count = null;
        try {
            //添加收藏到favorite表
            favoriteDao.addFavorite(rid, user);
            //更新route表中的数据
            favoriteDao.updateRouteCount(rid);

            //获取route表中的count的值
            count = favoriteDao.findRouteCount(rid);
        } catch (Exception e) {
            e.printStackTrace();
            count = 0;
        }
        return count;
    }

    @Override
    public PageBean<Favorite> findMyFavorite(Integer currentPage, Integer pageSize, User user) throws InvocationTargetException, IllegalAccessException {
        PageBean<Favorite> pageBean = new PageBean<>();
        //设置当前页以及每页数量
        pageBean.setCurrentPage(currentPage);
        pageBean.setPageSize(pageSize);
        //求得总数量
        Integer totalSize = favoriteDao.findMyFavoriteCount(user);
        //设置总数量
        pageBean.setTotalSize(totalSize);
        //获取路线集合
        List<Favorite> list = favoriteDao.findMyFavoriteList(currentPage, pageSize, user);
        pageBean.setList(list);
        return pageBean;
    }

    @Override
    public PageBean<Route> findFavoriterank(Integer currentPage, Integer pageSize, String rname, String minPrice, String maxPrice) {
        PageBean<Route> pageBean = new PageBean<>();
        //设置当前页以及每页数量
        pageBean.setCurrentPage(currentPage);
        pageBean.setPageSize(pageSize);
        //求得总数量
        Integer totalSize = favoriteDao.findFavoriterankCount(rname, minPrice, maxPrice);
        //设置总数量
        pageBean.setTotalSize(totalSize);
        //获取路线集合
        List<Route> list = favoriteDao.findFavoriterankList(currentPage, pageSize, rname, minPrice, maxPrice);
        pageBean.setList(list);
        return pageBean;
    }
}
