package com.xy.travel.service;

import com.xy.travel.pojo.Favorite;
import com.xy.travel.pojo.PageBean;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.User;

import java.lang.reflect.InvocationTargetException;

public interface FavoriteService {
    Favorite findIsFavorite(String rid, User user);

    Integer addFavorite(String rid, User user);

    PageBean<Favorite> findMyFavorite(Integer currentPage, Integer pageSize, User user) throws InvocationTargetException, IllegalAccessException;

    PageBean<Route> findFavoriterank(Integer currentPage, Integer pageSize, String rname, String minPrice, String maxPrice);
}
