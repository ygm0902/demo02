package com.xy.travel.constant;

public class Constant {
    public static final String SERVER_ERROR = "服务器异常";
    public static final String UNACTIVE = "N";
    public static final String ACTIVE = "Y";
    public static final String USER_KEY = "user";
    public static final String SESSION_CHECK = "CHECKCODE_SERVER";
    public static final String ERROR_CHECK = "验证码错误！";
}
