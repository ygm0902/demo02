package com.xy.travel.web.servlet;

import com.xy.travel.constant.Constant;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.Category;
import com.xy.travel.pojo.Result;
import com.xy.travel.service.CategoryService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet("/category")
public class CategoryServlet extends BaseServlet {

    private CategoryService categoryService = (CategoryService) BeanFactory.getBean("categoryService");

    private Result findAllCategory(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        try {
            //调用业务层
            List<Category> categoryList = categoryService.findAll();
            result.setData(categoryList);
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }
}
