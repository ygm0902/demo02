package com.xy.travel.web.servlet;

import com.xy.travel.constant.Constant;
import com.xy.travel.exception.*;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.Result;
import com.xy.travel.pojo.User;
import com.xy.travel.service.UserService;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet("/user")
public class UserServlet extends BaseServlet {
    private UserService userService = (UserService) BeanFactory.getBean("userService");

    /**
     * 校验用户名是否存在
     */
    private Result checkUsername(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //创建result对象
        Result result = new Result(true);
        //获取请求参数
        String username = request.getParameter("username");
        try {
            //调用业务层
            User user = userService.findUserByUsername(username);
            if (user == null) {
                result.setFlag(true);
            } else {
                result.setFlag(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }

    /**
     * 注册功能
     */
    private Result register(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //创建result对象
        Result result = new Result();
        /**
         * 校验验证码
         */
        //获得用户输入的验证码
        String check = request.getParameter("check");
        //获得服务器产生的验证码
        HttpSession session = request.getSession();
        String session_check = (String) session.getAttribute(Constant.SESSION_CHECK);
        if (session_check.equalsIgnoreCase(check)) {
            //获取所有请求参数
            Map<String, String[]> map = request.getParameterMap();
            //创建user对象
            User user = new User();
            //封装
            try {
                BeanUtils.populate(user, map);
                //调用业务层
                userService.doRegister(user);
                result.setFlag(true);
            } catch (Exception e) {
                e.printStackTrace();
                result.setFlag(false);
                result.setErrorMsg(Constant.SERVER_ERROR);
            }
        } else {
            result.setFlag(false);
            result.setErrorMsg(Constant.ERROR_CHECK);
        }
        return result;
    }

    /**
     * 邮箱激活
     */
    private Result active(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获取请求参数
        String code = request.getParameter("code");
        //调用业务层
        PrintWriter writer = response.getWriter();
        try {
            userService.active(code);
            writer.write("激活成功！");
        } catch (AlreadyActiveException e) {
            e.printStackTrace();
            writer.write("请勿重复激活");
        } catch (ErrorCodeException e) {
            e.printStackTrace();
            writer.write("激活码被篡改");
        } catch (Exception e) {
            e.printStackTrace();
            //说明激活失败
            writer.write("激活失败请重试");
        }
        return null;
    }

    /**
     * 登录功能
     */
    private Result login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Result result = new Result(false);
        //分别获取用户名 密码 验证码  后期需要分别对他们进行验证
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String check = request.getParameter("check");

        //获取系统生成的验证码
        HttpSession session = request.getSession();
        String session_check = (String) session.getAttribute(Constant.SESSION_CHECK);
        if (session_check.equalsIgnoreCase(check)) {
            //调用业务层 判断用户名和密码是否正确
            User user = null;
            try {
                user = userService.doLogin(username, password);
                session.setAttribute(Constant.USER_KEY, user);
                result.setFlag(true);
            } catch (UnActivedException e) {
                e.printStackTrace();
                result.setErrorMsg("用户未激活！");
            } catch (ErrorPasswordException e) {
                e.printStackTrace();
                result.setErrorMsg("密码错误");
            } catch (ErrorUsernameException e) {
                e.printStackTrace();
                result.setErrorMsg("用户名不存在！");
            }
        } else {
            result.setErrorMsg("验证码错误");
        }
        return result;
    }

    /**
     * 显示用户名
     */
    private Result findUserInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Constant.USER_KEY);
        Result result = new Result(true, user, null);
        return result;
    }

    /**
     * 退出登录
     */
    private Result logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().invalidate();
        Result result = new Result(true, null, null);
        return result;
    }
}
