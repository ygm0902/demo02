package com.xy.travel.web.servlet;

import com.xy.travel.constant.Constant;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.*;
import com.xy.travel.service.FavoriteService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/favorite")
public class FavoriteServlet extends BaseServlet {
    private FavoriteService favoriteService = (FavoriteService) BeanFactory.getBean("favoriteService");

    /**
     * 判断是否收藏
     *
     * @param request
     * @param response
     * @return
     */
    private Result isFavorite(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        //获取请求参数
        String rid = request.getParameter("rid");
        try {
            //获取user对象
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(Constant.USER_KEY);
            if (user != null) {
                //调用业务层
                Favorite favorite = favoriteService.findIsFavorite(rid, user);
                if (favorite != null) {
                    result.setData(true);
                } else {
                    result.setData(false);
                }
            } else {
                //代表未登录，显示未收藏
                result.setData(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        System.out.println("data:" + result.getData());
        return result;
    }

    /**
     * 添加收藏
     *
     * @param request
     * @param response
     * @return
     */
    private Result addFavorite(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        //获取请求参数
        String rid = request.getParameter("rid");
        try {
            //获取user
            User user = (User) request.getSession().getAttribute(Constant.USER_KEY);
            if (user != null) {
                //调用业务层
                Integer count = favoriteService.addFavorite(rid, user);
                result.setData(count);
            } else {
                result.setData(-1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }

    /**
     * 查看我的收藏
     *
     * @param request
     * @param response
     * @return
     */
    private Result findMyFavorite(HttpServletRequest request, HttpServletResponse response) {
        //获取请求参数
        Integer currentPage = Integer.valueOf(request.getParameter("currentPage"));
        Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));

        User user = (User) request.getSession().getAttribute(Constant.USER_KEY);
        try {
            //调用业务层
            PageBean<Favorite> pageBean = favoriteService.findMyFavorite(currentPage, pageSize, user);
            return new Result(true, pageBean, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, null, Constant.SERVER_ERROR);
        }
    }

    /**
     * 查看收藏排行榜
     *
     * @param request
     * @param response
     * @return
     */
    private Result findFavoriterank(HttpServletRequest request, HttpServletResponse response) {
        //获取请求参数
        Integer currentPage = Integer.valueOf(request.getParameter("currentPage"));
        Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
        String rname = request.getParameter("rname");
        String minPrice = request.getParameter("minPrice");
        String maxPrice = request.getParameter("maxPrice");

        try {
            //调用业务层
            PageBean<Route> pageBean = favoriteService.findFavoriterank(currentPage, pageSize, rname, minPrice, maxPrice);
            return new Result(true, pageBean, null);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, null, Constant.SERVER_ERROR);
        }
    }

}
