package com.xy.travel.web.servlet;

import com.xy.travel.constant.Constant;
import com.xy.travel.factory.BeanFactory;
import com.xy.travel.pojo.PageBean;
import com.xy.travel.pojo.Result;
import com.xy.travel.pojo.Route;
import com.xy.travel.service.RouteService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@WebServlet("/route")
public class RouteServlet extends BaseServlet {
    private RouteService routeService = (RouteService) BeanFactory.getBean("routeService");

    private Result careChoose(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        try {
            //调用业务层
            Map<String, List<Route>> map = routeService.careChoose();
            result.setData(map);
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }

    private Result findByPage(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        //获取请求参数
        String cid = request.getParameter("cid");
        Integer currentPage = Integer.valueOf(request.getParameter("currentPage"));
        Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
        String keyword = request.getParameter("keyword");

        try {
            //调用业务层的方法
            PageBean<Route> pageBean = routeService.findByPage(cid, currentPage, pageSize, keyword);
            result.setData(pageBean);
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }

    private Result findById(HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result(true);
        //获取请求参数
        String rid = request.getParameter("rid");
        try {
            //调用业务层
            Route route = routeService.findByRid(rid);
            result.setData(route);
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setErrorMsg(Constant.SERVER_ERROR);
        }
        return result;
    }

}
