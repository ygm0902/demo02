package com.xy.travel.pojo;

import java.util.List;

public class PageBean<T> {
    private Integer pageSize;
    private Integer currentPage;
    private Integer totalSize;
    private Integer totalPage;
    private List<T> list;

    @Override
    public String toString() {
        return "PageBean{" +
                "pageSize=" + pageSize +
                ", currentPage=" + currentPage +
                ", totalSize=" + totalSize +
                ", totalPage=" + totalPage +
                ", list=" + list +
                '}';
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
        //求得总页数
        this.totalPage = totalSize % pageSize == 0 ? totalSize / pageSize : totalSize / pageSize + 1;

    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
