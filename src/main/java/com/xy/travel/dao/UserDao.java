package com.xy.travel.dao;

import com.xy.travel.pojo.User;

public interface UserDao {

    User findUserByUsername(String username);

    void saveUser(User user);

    User findUserByCode(String code);

    void updateUser(User user);
}
