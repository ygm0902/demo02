package com.xy.travel.dao;

import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.RouteImg;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface RouteDao {
    List<Route> findPopList();

    List<Route> findNewestList();

    List<Route> findThemeList();

    Integer findTotalSize(String cid, String keyword);

    List<Route> findRouteListByPage(String cid, Integer currentPage, Integer pageSize, String keyword);

    Route findByRid(String rid) throws InvocationTargetException, IllegalAccessException;

    List<RouteImg> findRouteImgListByRid(String rid);
}
