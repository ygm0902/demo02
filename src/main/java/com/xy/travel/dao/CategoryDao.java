package com.xy.travel.dao;

import com.xy.travel.pojo.Category;

import java.util.List;

public interface CategoryDao {
    List<Category> findAll();

}
