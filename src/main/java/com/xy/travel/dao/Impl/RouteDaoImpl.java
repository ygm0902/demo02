package com.xy.travel.dao.Impl;

import com.xy.travel.dao.RouteDao;
import com.xy.travel.factory.JDBCUtil;
import com.xy.travel.pojo.Category;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.RouteImg;
import com.xy.travel.pojo.Seller;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RouteDaoImpl implements RouteDao {
    private JdbcTemplate template = new JdbcTemplate(JDBCUtil.getDataSource());

    @Override
    public List<Route> findPopList() {
        String sql = "SELECT * FROM tab_route WHERE rflag=1 ORDER BY COUNT DESC LIMIT 0,4";
        List<Route> routeList = null;
        try {
            routeList = template.query(sql, new BeanPropertyRowMapper<>(Route.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return routeList;
    }

    @Override
    public List<Route> findNewestList() {
        String sql = "SELECT * FROM tab_route WHERE rflag=1 ORDER BY rdate DESC LIMIT 0,4";
        List<Route> routeList = template.query(sql, new BeanPropertyRowMapper<>(Route.class));
        return routeList;
    }

    @Override
    public List<Route> findThemeList() {
        String sql = "select * from tab_route where rflag=1 AND isThemeTour=1 limit 0,4";
        List<Route> routeList = template.query(sql, new BeanPropertyRowMapper<>(Route.class));
        return routeList;
    }

    @Override
    public Integer findTotalSize(String cid, String keyword) {
        String sql = "select count(*) from tab_route where rflag=1";
        List<Object> params = new ArrayList<>();
        if (cid != null && !cid.equals("") && !"null".equals(cid)) {
            sql += " and cid=?";
            params.add(cid);
        }
        if (keyword != null && !keyword.equals("") && !"null".equals(keyword)) {
            sql += " and rname like?";
            params.add("%" + keyword + "%");
        }
        Integer totalSize = template.queryForObject(sql, Integer.class, params.toArray());
        return totalSize;
    }

    @Override
    public List<Route> findRouteListByPage(String cid, Integer currentPage, Integer pageSize, String keyword) {
        String sql = "select * from tab_route where rflag=1";
        List<Object> params = new ArrayList<>();
        if (cid != null && !cid.equals("") && !"null".equals(cid)) {
            sql += " and cid=?";
            params.add(cid);
        }
        if (keyword != null && !keyword.equals("") && !"null".equals(keyword)) {
            sql += " and rname like?";
            params.add("%" + keyword + "%");
        }

        sql += " limit ?,?";
        params.add((currentPage - 1) * pageSize);
        params.add(pageSize);
        List<Route> routeList = template.query(sql, new BeanPropertyRowMapper<>(Route.class), params.toArray());
        return routeList;
    }

    @Override
    public Route findByRid(String rid) throws InvocationTargetException, IllegalAccessException {
        String sql = "SELECT * FROM tab_route r,tab_category c,tab_seller s WHERE r.cid = c.cid AND r.sid = s.sid AND rid =?";
        Map<String, Object> map = template.queryForMap(sql, rid);
        Route route = new Route();
        BeanUtils.populate(route, map);

        Category category = new Category();
        BeanUtils.populate(category, map);
        route.setCategory(category);

        Seller seller = new Seller();
        BeanUtils.populate(seller, map);
        route.setSeller(seller);

        return route;
    }

    @Override
    public List<RouteImg> findRouteImgListByRid(String rid) {
        String sql = "SELECT * FROM tab_route_img WHERE rid=?";
        List<RouteImg> routeList = template.query(sql, new BeanPropertyRowMapper<>(RouteImg.class), rid);
        return routeList;
    }
}
