package com.xy.travel.dao.Impl;

import com.xy.travel.dao.CategoryDao;
import com.xy.travel.factory.JDBCUtil;
import com.xy.travel.pojo.Category;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CategoryDaoImpl implements CategoryDao {
    private JdbcTemplate template = new JdbcTemplate(JDBCUtil.getDataSource());

    @Override
    public List<Category> findAll() {
        String sql = "select * from tab_category";
        List<Category> categoryList = null;
        try {
            categoryList = template.query(sql, new BeanPropertyRowMapper<>(Category.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return categoryList;
    }
}
