package com.xy.travel.dao.Impl;

import com.xy.travel.dao.FavoriteDao;
import com.xy.travel.factory.JDBCUtil;
import com.xy.travel.pojo.Favorite;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.User;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FavoriteDaoImpl implements FavoriteDao {
    private JdbcTemplate template = new JdbcTemplate(JDBCUtil.getDataSource());

    @Override
    public Favorite findIsFavorite(String rid, User user) {
        String sql = "select * from tab_favorite where rid=? and uid=?";
        Favorite favorite = null;
        try {
            favorite = template.queryForObject(sql, new BeanPropertyRowMapper<>(Favorite.class), rid, user.getUid());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return favorite;
    }

    @Override
    public void addFavorite(String rid, User user) {
        String sql = "insert into tab_favorite values(?,?,?)";
        template.update(sql, rid, new Date(), user.getUid());
    }

    @Override
    public void updateRouteCount(String rid) {
        String sql = "update tab_route set count=count+1 where rid=?";
        template.update(sql, rid);
    }

    @Override
    public Integer findRouteCount(String rid) {
        String sql = "select count from tab_route where rid=?";
        Integer count = template.queryForObject(sql, Integer.class, rid);
        return count;
    }

    @Override
    public Integer findMyFavoriteCount(User user) {
        String sql = "select count(*) from tab_favorite where uid=?";
        Integer totalSize = template.queryForObject(sql, Integer.class, user.getUid());
        return totalSize;
    }

    @Override
    public List<Favorite> findMyFavoriteList(Integer currentPage, Integer pageSize, User user) throws InvocationTargetException, IllegalAccessException {
        String sql = "SELECT * FROM tab_route r,tab_favorite f WHERE r.rid=f.rid AND uid=? limit ?,?";
        List<Map<String, Object>> list = template.queryForList(sql, user.getUid(), (currentPage - 1) * pageSize, pageSize);
        List<Favorite> favoriteList = new ArrayList<>();
        for (Map<String, Object> map : list) {
            Favorite favorite = new Favorite();
            BeanUtils.populate(favorite, map);

            Route route = new Route();
            BeanUtils.populate(route, map);
            favorite.setRoute(route);

            favorite.setUser(user);

            favoriteList.add(favorite);
        }
        return favoriteList;
    }

    @Override
    public Integer findFavoriterankCount(String rname, String minPrice, String maxPrice) {
        String sql = "select count(*) from tab_route where rflag=1";
        List<Object> params = new ArrayList<>();
        if (rname != null && !"".equals(rname) && !"null".equals(rname)) {
            sql += " and rname like ?";
            params.add("%" + rname + "%");
        }
        if (minPrice != null && !"".equals(minPrice) && !"null".equals(minPrice)) {
            sql += " and price>= ?";
            params.add(minPrice);
        }
        if (maxPrice != null && !"".equals(maxPrice) && !"null".equals(maxPrice)) {
            sql += " and price<= ?";
            params.add(maxPrice);
        }
        Integer totalSize = template.queryForObject(sql, Integer.class, params.toArray());
        return totalSize;
    }

    @Override
    public List<Route> findFavoriterankList(Integer currentPage, Integer pageSize, String rname, String minPrice, String maxPrice) {
        String sql = "select * from tab_route where rflag=1";
        List<Object> params = new ArrayList<>();
        if (rname != null && !"".equals(rname) && !"null".equals(rname)) {
            sql += " and rname like ?";
            params.add("%" + rname + "%");
        }
        if (minPrice != null && !"".equals(minPrice) && !"null".equals(minPrice)) {
            sql += " and price>= ?";
            params.add(minPrice);
        }
        if (maxPrice != null && !"".equals(maxPrice) && !"null".equals(maxPrice)) {
            sql += " and price<= ?";
            params.add(maxPrice);
        }
        sql += " order by count desc limit ?,?";
        params.add((currentPage - 1) * pageSize);
        params.add(pageSize);
        List<Route> routeList = template.query(sql, new BeanPropertyRowMapper<>(Route.class), params.toArray());
        return routeList;
    }
}
