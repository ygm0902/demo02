package com.xy.travel.dao;

import com.xy.travel.pojo.Favorite;
import com.xy.travel.pojo.Route;
import com.xy.travel.pojo.User;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface FavoriteDao {
    Favorite findIsFavorite(String rid, User user);

    void addFavorite(String rid, User user);

    void updateRouteCount(String rid);

    Integer findRouteCount(String rid);

    Integer findMyFavoriteCount(User user);

    List<Favorite> findMyFavoriteList(Integer currentPage, Integer pageSize, User user) throws InvocationTargetException, IllegalAccessException;

    Integer findFavoriterankCount(String rname, String minPrice, String maxPrice);

    List<Route> findFavoriterankList(Integer currentPage, Integer pageSize, String rname, String minPrice, String maxPrice);
}
